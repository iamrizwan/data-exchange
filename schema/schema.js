const graphql = require ('graphql');
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
} = graphql;
const _ = require ('lodash');
const axios = require ('axios');
const sha256 = require ('js-sha256');
const GraphQLJSON = require ('graphql-type-json');
const users = [
  {id: '1', firstname: 'rizwan', age: 23},
  {id: '2', firstname: 'rizwan2', age: 24},
  {id: '3', firstname: 'rizwan3', age: 25},
  {id: '4', firstname: 'rizwan4', age: 26},
];

const Scheme1 = new GraphQLObjectType ({
  name: 'Scheme1',
  fields: {
    id: {type: GraphQLString},
    AADHAAR_NUMBER: {type: GraphQLString},
    BENEFICIARY_NAME: {type: GraphQLString},
    GENDER: {type: GraphQLString},
    MEMBER_AGE: {type: GraphQLString},
    ADDRESS: {type: GraphQLString},
    DIST_NAME: {type: GraphQLString},
    SCHEME_NAME: {type: GraphQLString},
  },
});
const Scheme2 = new GraphQLObjectType ({
  name: 'Scheme2',
  fields: {
    id: {type: GraphQLString},
    UDAI: {type: GraphQLString},
    firstName: {type: GraphQLString},
    sex: {type: GraphQLString},
    age: {type: GraphQLString},
    address: {type: GraphQLString},
    city: {type: GraphQLString},
    SCHEME_NAME: {type: GraphQLString},
  },
});
const LogType = new GraphQLObjectType ({
  name: 'LogType',
  fields: {
    id: {type: GraphQLString},
    data: {type: GraphQLJSON},
    from: {type: GraphQLString},
    to: {type: GraphQLString},
    DataHash: {type: GraphQLString},
    timestamp: {type: GraphQLString},
    nodeId: {type: GraphQLString},
    type: {type: GraphQLString},
    exchangeRequest: {type: GraphQLString},
    txID: {type: GraphQLString},
    txReceipt: {type: GraphQLString},
    contractId: {type: GraphQLString},
  },
});

const RootQuery = new GraphQLObjectType ({
  name: 'RootQueryType',
  fields: {
    getD1Beneficiary: {
      type: Scheme1,
      args: {id: {type: GraphQLString}},
      resolve (parentValue, args) {
        return axios
          .get (`http://localhost:3000/scheme1/${args.id}`)
          .then (res => res.data);
      },
    },
    getD1AllBeneficiary: {
      type: new GraphQLList (Scheme1),
      args: {id: {type: GraphQLString}},
      resolve (parentValue, args) {
        return axios
          .get (`http://localhost:3000/scheme1`)
          .then (res => res.data);
      },
    },
    getD2Beneficiary: {
      type: Scheme2,
      args: {id: {type: GraphQLString}},
      resolve (parentValue, args) {
        return axios
          .get (`http://localhost:3000/scheme2/${args.id}`)
          .then (res => res.data);
      },
    },
    getD2AllBeneficiary: {
      type: new GraphQLList (Scheme2),
      args: {id: {type: GraphQLString}},
      resolve (parentValue, args) {
        return axios
          .get (`http://localhost:3000/scheme2`)
          .then (res => res.data);
      },
    },
    requestBeneficiaryFromD1: {
      type: Scheme1,
      args: {id: {type: GraphQLString}, token: {type: GraphQLString}},
      resolve (parentValue, args) {
        if (!args.id || args.id === '') {
          throw ' put beneficiary id';
        }
        if (args.token) {
          return new Promise ((resolve, reject) => {
            axios
              .get (`http://localhost:3000/scheme1/${args.id}`)
              .then (res => {
                let data = {
                  from: 'Depart1',
                  to: 'Depart2',
                  data: res.data,
                  exchangeRequest: 'GetDataAsset',
                  timestamp: new Date (),
                  DataHash: sha256(JSON.stringify (res.data)),
                  nodeId:"66a34bd0-f4e7-11e7-a52b-016a36a9d789",
                  type:'GraphChain v.07',
                  txID:sha256(JSON.stringify (res.data.AADHAAR_NUMBER)),
                  txReceipt:sha256(JSON.stringify (res.data.BENEFICIARY_NAME)),
                  contractId:'0x4v3148af01bb5683ed599d00369017dd45e18c20'
                };
                console.log (data);
                axios.post (`http://localhost:3000/logs/`, data);
                resolve (res.data);
              });
          });
        } else {
          throw 'You dont have permisson';
        }
      },
    },
    requestBeneficiaryFromD2: {
      type: Scheme2,
      args: {id: {type: GraphQLString}, token: {type: GraphQLString}},
      resolve (parentValue, args) {
        if (!args.id || args.id === '') {
          throw ' put beneficiary id';
        }
        if (args.token) {
          return new Promise ((resolve, reject) => {
            axios
              .get (`http://localhost:3000/scheme2/${args.id}`)
              .then (res => {
                let data = {
                  from: 'Depart2',
                  to: 'Depart1',
                  data: res.data,
                  exchangeRequest: 'GetDataAsset',
                  timestamp: new Date (),
                  DataHash: sha256(JSON.stringify (res.data)),
                  nodeId:"66a34bd0-f4e7-11e7-a52b-016a36a9d789",
                  type:'GraphChain v.07',
                  txID:sha256(JSON.stringify (res.data.UDAI)),
                  txReceipt:sha256(JSON.stringify (res.data.firstName)),
                  contractId:'0x4v3148af01bb5683ed599d00369017dd45e18c20'
                };
                console.log (data);
                axios.post (`http://localhost:3000/logs/`, data);
                resolve (res.data);
              });
          });
        } else {
          throw 'you dont have permisson';
        }
      },
    },
    getLogs: {
      type: new GraphQLList (LogType),
      args: {id: {type: GraphQLString}},
      resolve (parentValue, args) {
        return axios.get (`http://localhost:3000/logs`).then (res => res.data);
      },
    },
    getLogsByUDAI: {
      type: new GraphQLList (LogType),
      args: {id: {type: GraphQLString}},
      resolve (parentValue, args) {
        return axios.get (`http://localhost:3000/logs`).then (res => res.data);
      },
    },
  },
});

module.exports = new GraphQLSchema ({
  query: RootQuery,
});
