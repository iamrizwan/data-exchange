var express = require ('express');
const expressGraphQl = require ('express-graphql');
const app = express ();
const schema = require('./schema/schema')
require('./jsonserver.js')
app.use (
  '/chips',
  expressGraphQl ({
    graphiql: true,
    schema
  })
);

app.get('/',(req,res)=>{
    res.send("hellow orld")
})

app.listen (4000, () => {
  console.log ('graph-chain server is listening at port 4000');
});
